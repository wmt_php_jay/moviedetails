import React, { Component } from 'react'
import axios from 'axios';
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
import CardDesign from '../componet/CardDesign'
var url;
export class TopRated extends Component {


    state = {
        top_movies: {

        },
        current: 1
    }

    async componentDidMount() {

        await this.getData();

    }
    onchange = async (page) => {
        await this.setState({
            current: page,
        });
        this.getData();
    }

    getData() {
        url = 'https://api.themoviedb.org/3/movie/top_rated?api_key=c3d479809451b8725eccd68c8cbe9f13&page=' + encodeURI(this.state.current);

        axios.get(url).then(res => this.setState({ top_movies: res.data.results }));
    }

    render() {
        var data1 = Object.keys(this.state.top_movies).map(i => this.state.top_movies[i]);

        return (
            <div>
                <Pagination className="pagination" onChange={this.onchange} current={this.state.current} total={3042} />

                <div className="box">
                    <div className="container">
                        <div className="row">

                            {data1.map((i, key) => {
                                return <div className="col-md-3" key={key}><CardDesign title={i.title} poster_path={i.poster_path} vote_average={i.vote_average} vote_count={i.vote_count} id={i.id} /></div>

                            })}

                        </div>
                    </div>
                </div>
            </div>
        )



    }
}

export default TopRated
