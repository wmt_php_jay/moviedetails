import React, { Component } from 'react'

export class Header extends Component {


    render() {

        return (


            <nav className="navbar  navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="/">Movie Site</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/DisplayPopularMovie">Popular Movies</a>
                        </li>
                        <li className="nav-item ">
                            <a className="nav-link" href="/TopRated">TopRated Movies</a>
                        </li>

                    </ul>

                </div>
            </nav>

        )
    }
}

export default Header
