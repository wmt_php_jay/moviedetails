import React, { Component } from 'react'
import MovieDetailsDesign from '../componet/MovieDetailsDesign'
import axios from 'axios';
var i, j, id;
var data1 = "https://api.themoviedb.org/3/movie";
var data2 = "api_key=c3d479809451b8725eccd68c8cbe9f13&language=en-US"
export class MovieDetails extends Component {

  constructor(props) {
    super(props)
    i = this.props.location.pathname;
    j = i.split('/');
    id = j[2];
  }

  state = {
    moviedetails: {

    }
  }
  
     
 
  async componentDidMount() {

   await axios.get(`${data1}/${id}?${data2}`).then(res => this.setState({ moviedetails: res.data }));
  }

  render() {
    return (
      <div>
        <MovieDetailsDesign vote_average={this.state.moviedetails.vote_average} poster_path={this.state.moviedetails.poster_path} original_title={this.state.moviedetails.original_title} overview={this.state.moviedetails.overview} release_date={this.state.moviedetails.release_date} runtime={this.state.moviedetails.runtime} status={this.state.moviedetails.status} revenue={this.state.moviedetails.revenue} votecount={this.state.moviedetails.vote_count} tagline={this.state.moviedetails.tagline} budget={this.state.moviedetails.budget} />
      </div>
    )
  }
}

export default MovieDetails
