import React, { Component } from 'react'
import axios from 'axios';
import CardDesign from './CardDesign'
import Pagination from 'rc-pagination';
import 'rc-pagination/assets/index.css';
var url;
export class DisplayPopularMovie extends Component {
    constructor(props) {
        super(props);

        this.getData = this.getData.bind(this);

    }
    state = {

        movies: {

        },
        current: 1

    }


    async componentDidMount() {


        await this.getData()


    }
    onchange = async (page) => {
        await this.setState({
            current: page,
        });
        this.getData();
    }
    getData() {
        url = 'https://api.themoviedb.org/3/movie/popular?api_key=c3d479809451b8725eccd68c8cbe9f13&page=' + encodeURI(this.state.current);
        axios.get(url).then(res => this.setState({ movies: res.data.results }));
    }

    render() {
        var data1 = Object.keys(this.state.movies).map(i => this.state.movies[i]);
    
        return (
            <div>
                <Pagination className="pagination" onChange={this.onchange} current={this.state.current} total={9037} />
                
                    <div className="container">
                        <div className="row">

                            {data1.map((i, key) => {
                                return <div className="col-md-3" key={key}><CardDesign title={i.title} poster_path={i.poster_path} vote_average={i.vote_average} vote_count={i.vote_count} id={i.id} /></div>
                                // { return <div key={key}>{i}</div> }
                            })}



                        </div>
                        <div>


                        </div>
                    </div>

                </div>
           
        )



    }
}

export default DisplayPopularMovie

