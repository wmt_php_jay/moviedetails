import React from 'react'
import notfound from '../images/notfound.png'

var data;
function ConvertMin(mins){
let h = Math.floor(mins / 60);
  let m = mins % 60;
  h = h < 10 ? '0' + h : h;
  m = m < 10 ? '0' + m : m;
  return `${h}h:${m}m`;
}
function ConverUSD (labelValue) 
  {

      return Math.abs(Number(labelValue)) >= 1.0e+9
  
      ? Math.abs(Number(labelValue)) / 1.0e+9 + " Billions"
     
      : Math.abs(Number(labelValue)) >= 1.0e+6
  
      ? Math.abs(Number(labelValue)) / 1.0e+6 + " Millions"
     
      : Math.abs(Number(labelValue)) >= 1.0e+3
  
      ? Math.abs(Number(labelValue)) / 1.0e+3 + " K"
  
      : Math.abs(Number(labelValue));
  }

function MovieDetailsDesign(i) {

  if (i.poster_path === null) {
    data = notfound;
  }
  else {
    data = `https://image.tmdb.org/t/p/w500${i.poster_path}`;
  }

  var revenue = ConverUSD(i.revenue);
  var budget = ConverUSD(i.budget);
  var hours = ConvertMin(i.runtime);

  return (
    <div>
      <div className="container">
        <div className="cardDetails">
          <div className="container-fliud">
            <div className="wrapper row">
              <div className="preview col-md-6">

                <div className="preview-pic tab-content">
                  <div className="tab-pane active" id="pic-1">   <img src={data} className="poster img-rounded" alt={i.original_title} /></div>
                </div>
              </div>
              <div className="details col-md-6">
                <h3>{i.original_title}</h3>
                <h5>{i.tagline}</h5>
                <p className="pt-2">{i.overview}</p>
                
                {/* <StarRatings
                   starDimension="20px"
                   starSpacing="1px"
                  rating={i.vote_average}
                  starRatedColor="#FDAB41"
                  numberOfStars={5}
                  name='rating'
                /> */}
                <hr/>
                <div className="inline">
                  <span>Revenue- </span><span className="font-weight-bold">{revenue.toString()}</span>
                  </div>
                  <div className="inline">
                  <span>Budget- </span> <span className="font-weight-bold"> {budget}</span>
                  </div>
                  <div className="inline">
                  <span>Duration- </span> <span className="font-weight-bold">{hours}</span>
                  </div>
                
                  <div className="inline">
                  <span>Status- </span> <span className="font-weight-bold">{i.status}</span>
                  </div>
                  <div className="inline">
                  <span>Releasedate- </span> <span className="font-weight-bold">{i.release_date}</span>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}



export default MovieDetailsDesign
