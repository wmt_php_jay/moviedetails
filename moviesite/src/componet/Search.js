import React, { Component } from 'react'
import axios from 'axios';
import CardDesign from '../componet/CardDesign'

var url = '';
export class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: [],
            term: 'abc',
            loding: true

        };


        this.clear();
        this.changeTerm = this.changeTerm.bind(this);
    }

    async clear() {

        url = 'https://api.themoviedb.org/3/trending/all/day?api_key=c3d479809451b8725eccd68c8cbe9f13';
        axios.get(url).then(res => this.setState({ results: res.data.results }));
    }
    async changeTerm(event) {

        if (event.target.value === '') {
            this.setState.results = '';
            await this.clear();
        } else {
            await this.setState({ term: event.target.value});
            let url = 'https://api.themoviedb.org/3/search/movie?api_key=c3d479809451b8725eccd68c8cbe9f13&query=' + encodeURI(this.state.term);
            axios.get(url)
                .then(response => {
                    let data = {
                        results: response.data.results,
                    };
                    this.setState(data);
                })
                .catch(error => console.log(error));
        }
    }

    render() {

        var data1 = Object.keys(this.state.results).map(i => this.state.results[i]);
        var data = data1.filter(data1 => data1 !== 'null' || data1 !== 'undifined');
       
        return (
            <div>
                <div className="search">

                    <form>
                        <input className="form-control" type="search" placeholder="Search" aria-label="Search" onChange={this.changeTerm} />

                    </form>
                </div>


                <div className="box">
                    <div className="container">
                        <div className="row">

                            {data.map((i, key) => {

                                return <div className="col-md-3" key={key}><CardDesign title={i.title} poster_path={i.poster_path} vote_average={i.vote_average} vote_count={i.vote_count} id={i.id} /></div>

                            })}

                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Search
