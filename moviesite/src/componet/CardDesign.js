import React from 'react'
import { Link } from "react-router-dom";
import star from '../images/star.png'
import like from '../images/like.png'
import notfound from '../images/notfound.png'

var data;
var titlenotfound;
function CardDesign(i) {
    if (i.poster_path === null) { data = notfound; } else { data = `https://image.tmdb.org/t/p/w500${i.poster_path}`; }
    if(i.title===undefined){titlenotfound="Not Found"}else{titlenotfound=i.title}

    return (
        <div className="card"><Link to={`/MovieDetails/${i.id}`}><img className="card-img-top" src={data} alt={data} /></Link><div className="card-body"><Link to={`/MovieDetails/${i.id}`}> <strong className="card-title">{titlenotfound}</strong></Link></div>
                <div>
            <p><img className="rating" src={star} alt="" /><span className="data">{i.vote_average}/10</span></p>
            <p><img className="like" src={like} alt="" /><span className="data">{i.vote_count}</span> </p></div>

        </div>
    )
}

export default CardDesign

