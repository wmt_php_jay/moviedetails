import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import DisplayPopularMovie from './componet/DisplayPopularMovie'
import MovieDetails from './pages/MovieDetails'
import Header from './pages/Header'
import TopRated from './pages/TopRated'
import Search from './componet/Search';
//import axios from 'axios';

class App extends Component {


  render() {

    return (

      <Router>

        <div className="App">
          <div>
            <Header />

            <Route exact path="/" render={props => (
           
              <div>
             
                <Search />
               
              </div>
            )} />

          
          </div>

          <Route path="/DisplayPopularMovie" component={DisplayPopularMovie} />
            <Route path="/TopRated" component={TopRated} />
            <Route path="/MovieDetails" component={MovieDetails} />
            <Route path="/Search" component={Search} />

        </div>
      </Router>
    );

  }
}

export default App;
